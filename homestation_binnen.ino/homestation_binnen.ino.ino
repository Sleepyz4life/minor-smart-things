#include <Arduino.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include <Wire.h>
#include <Time.h>
#include <SimpleTimer.h>
#include "SSD1306.h"
#include "OLEDDisplayUi.h"
#include "images.h"
#include <WiFiUDP.h>
#include <WakeOnLan.h>

class WeatherDataPackage {
  public:
  String userName;
  String updatedAt;
  float temperature;
  float humidity;
  float lightIntensity;
  boolean precipitation;
  float windSpeed;
  
  WeatherDataPackage(){ 
    //the constructor 
  }
};

class WOLDataPackage {
  public:
  int value;
  boolean received = true;
  
  WOLDataPackage(){ 
    //the constructor 
  }
};


HTTPClient http;
WeatherDataPackage wdp;
WOLDataPackage woldp;
SSD1306  display(0x3c, 4, 5);
OLEDDisplayUi ui ( &display );
SimpleTimer timer;

//Wake on land defines
WiFiUDP UDP;
IPAddress computer_ip(192, 168, 1, 100); 
byte mac[] = { 0x4C, 0xCC, 0x6A, 0xB0, 0xC9, 0x43 };

const char *ssid = "ZupahWifi";
const char *password = "awesomepw123!";
String iotUrl = "http://iot-open-server.herokuapp.com/data";
String actuatorUrl = "https://stud.hosted.hr.nl/0898095/weatherstation/getActuator.php";
String deviceId = "59b2627bf728010004dd610a";
int timezone = 3;
int dst = 0;

void msOverlay(OLEDDisplay *display, OLEDDisplayUiState* state) {
  time_t now = time(nullptr);
  
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->setFont(ArialMT_Plain_16);
  tm* timeinfo = localtime(&now);
  String hour = (String)(timeinfo->tm_hour);
  String minute = (String)timeinfo->tm_min;
  String second = (String)timeinfo->tm_sec;  
  String timestring = hour + ":" + minute + ":" + second;
  display->drawString(128, 0, String(timestring));
}

void connectFrame()
{
  display.clear();
  display.setFont(ArialMT_Plain_16);
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString( 0, 0, "Connecting to:");
  display.drawString( 0, 32, String(ssid));
  display.display();
}

void waitingFrame()
{
  display.clear();
  display.setFont(ArialMT_Plain_16);
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(0, 32, "Waiting for data...");
  display.display();
}

void updateFrame ()
{
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_16);
  display.drawString( 0, 0, "Updating data");
  display.display();
}

void drawFrame6 (OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y)
{
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_16);
  display->drawString(0, 0, "Temp");
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_24);
  display->drawString(0, 32, String(wdp.temperature, 1) + " " + "\xDF" + "C");
}

void drawFrame7 (OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y)
{
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_16);
  display->drawString(0, 0, "Humidity");
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_24);
  display->drawString(0, 32, String(wdp.humidity, 1) + "%");
}

void drawFrame8 (OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y)
{
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_16);
  display->drawString(0, 0, "Light");
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_24);
  display->drawString(0, 32, String(wdp.lightIntensity, 1) + "%");
}

void drawFrame9 (OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y)
{
  String precipitationString = "Dry";
  if(wdp.precipitation) {
    precipitationString = "Rain / snow";
  }
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_16);
  display->drawString(0, 0, "Weather");
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_24);
  display->drawString(0, 32, precipitationString);
}

void drawFrame10 (OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y)
{
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_16);
  display->drawString(0, 0, "WindSP");
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_24);
  display->drawString(0, 32, String(wdp.windSpeed, 1) + "RPM");
}

FrameCallback frames[] = { drawFrame6, drawFrame7, drawFrame8, drawFrame9, drawFrame10 };
int frameCount = 5;

// Overlays are statically drawn on top of a frame eg. a clock
OverlayCallback overlays[] = { msOverlay };
int overlaysCount = 1;

void setup() {
  Serial.begin(115200);
  Serial.flush();

  configTime(2 * 3600, 0, "pool.ntp.org", "time.nist.gov");

 // Customize the active and inactive symbol
  ui.setTargetFPS(60);  
    
  // You can change the transition that is used
  // SLIDE_LEFT, SLIDE_RIGHT, SLIDE_UP, SLIDE_DOWN
  ui.setFrameAnimation(SLIDE_LEFT);

  // Add frames
  ui.setFrames(frames, frameCount);

  // Add overlays
  ui.setOverlays(overlays, overlaysCount);

  ui.setTimePerFrame(2000);
  ui.disableAutoTransition();
  ui.disableAllIndicators();
  // Initialising the UI will init the display too.
  ui.init();
  display.flipScreenVertically();

  timer.setInterval(30000, GetDataFromApi);
  connectToWifiNetwork();  
}

void loop() {
  int remainingTimeBudget = ui.update();
  
  if (remainingTimeBudget > 0) { 
    timer.run();
    delay(remainingTimeBudget);
  }
}

void connectToWifiNetwork() {
  Serial.println ( "Trying to establish WiFi connection with " + String(ssid));
  //wifiMulti.addAP(ssid, password);
  connectFrame();
 
  WiFi.begin ( ssid, password );
  Serial.println ( "" );

  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }
  
  Serial.println ( "" );
  Serial.print ( "Connected to " );
  Serial.println ( ssid );
  Serial.print ( "IP address: " );
  Serial.println ( WiFi.localIP() );

  waitingFrame();

  GetDataFromApi();
}

String getData(String url) {
  ui.enableAutoTransition();
  bool failed = false;
  String payload;
  if(WiFi.status()== WL_CONNECTED){
    http.begin(url);
    int httpCode = http.GET();
    Serial.println(httpCode);
    if(httpCode == HTTP_CODE_OK) {
      payload = http.getString();
      Serial.println(payload);
    } else {
      failed = true;
    }
    http.end();
  } else {
    Serial.println("Wifi connection failed, retrying.");   
    connectToWifiNetwork();
  }
  if(!failed) {
     return payload;
  } else {
    return "";
  }
}

void dataToWeatherPackage(String dataString) {
  const size_t bufferSize = 10*JSON_ARRAY_SIZE(2) + 5*JSON_ARRAY_SIZE(3) + 5*JSON_ARRAY_SIZE(4) + 2*JSON_ARRAY_SIZE(5) + JSON_ARRAY_SIZE(6) + JSON_ARRAY_SIZE(7) + JSON_ARRAY_SIZE(24) + JSON_OBJECT_SIZE(1) + 25*JSON_OBJECT_SIZE(2) + 13*JSON_OBJECT_SIZE(3) + 66*JSON_OBJECT_SIZE(5) + 19*JSON_OBJECT_SIZE(6) + 11460;
  DynamicJsonBuffer jsonBuffer(bufferSize);
  JsonObject& dataRoot = jsonBuffer.parseObject(dataString);
  JsonArray& devices = dataRoot["devices"];
  for(int i = 0; i < devices.size(); i++) {
    JsonObject& device = devices[i];
    if(device["_id"].as<String>() == deviceId) {
      wdp.userName = device["name"].as<String>();
      wdp.updatedAt = device["updatedAt"].as<String>();
      wdp.temperature = device["data"][0]["value"].as<float>();
      wdp.humidity = device["data"][1]["value"].as<float>();
      wdp.lightIntensity = device["data"][2]["value"].as<float>();
      wdp.precipitation = device["data"][3]["value"].as<boolean>();
      wdp.windSpeed = device["data"][4]["value"].as<float>();
    }
  }
}

void dataToWOLPackage(String dataString) {
  DynamicJsonBuffer jsonBuffer(200);
  JsonArray& devices = jsonBuffer.parseArray(dataString);
  Serial.println("Aantal devices");
  Serial.println(devices.size());
  for(int i = 0; i < devices.size(); i++) {
    JsonObject& device = devices[i];
    if(device["device_id"].as<String>() == deviceId) {
      woldp.value = device["value"].as<int>();
      woldp.received = device["received"].as<boolean>();
    }
  }
}


void GetDataFromApi()
{
    updateFrame();
    String dataString = getData(iotUrl);
    if(dataString == "") {return;}
    dataToWeatherPackage(dataString);
    String receivedActuatorString = getData(actuatorUrl);
    dataToWOLPackage(receivedActuatorString);
    sendWOL();
    ui.switchToFrame(0);
}

void sendWOL() {
  if(woldp.received != 1) {
    Serial.println("Sending WOL Package ");
    WakeOnLan::sendWOL(computer_ip, UDP, mac, sizeof mac);
  }
}



