#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include "DHT.h"
 
#define DHTPIN D4
#define WEATHERPIN D3
#define DHTTYPE DHT22
#define LDRPIN A0
#define WINDPIN D2
 
DHT dht(DHTPIN, DHTTYPE);
HTTPClient http;
 
// const char *ssid = "Wi-Fi-netwerk van Tim";
// const char *password = "123456789wifivantim";
const char *ssid = "ZupahWifi";
const char *password = "awesomepw123!";
String httpPostUrl = "http://iot-open-server.herokuapp.com/data";
String apiToken = "7b9ab658814aa7c76597b034";
 
void connectToWifiNetwork() {
  Serial.println ( "Trying to establish WiFi connection" );
  WiFi.begin ( ssid, password );
  Serial.println ( "" );
   
  // Wait for connection
  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }
 
  Serial.println ( "" );
  Serial.print ( "Connected to " );
  Serial.println ( ssid );
  Serial.print ( "IP address: " );
  Serial.println ( WiFi.localIP() );
}
 
float readLightInensity() {
  float ldrIn = analogRead(LDRPIN);
  return ldrIn / 10;
}
 
float readRain() {
  bool rainIn = digitalRead(WEATHERPIN);
  return !rainIn;
}
 
float readWindSpeed() {
  int state, prevState = 0;
  int counter = 0;
  int timeToRead = 1000;
  float startTime = millis();

  while((millis() - startTime) < timeToRead) {
    state = digitalRead(WINDPIN);
    delay(1); //Hacky solution to prevent false readings

    if(state != prevState)
    {
      counter++;
      prevState = state;
    }
  }

  counter = counter / 2; //= rounds p sec
  int rpm = counter * (60000 / timeToRead);
  return rpm; //should become km/h
}
 
void postData(String stringToPost) {
  if(WiFi.status()== WL_CONNECTED){
    http.begin(httpPostUrl);
    http.addHeader("Content-Type", "application/json");
    int httpCode = http.POST(stringToPost);
    String payload = http.getString();
   
    Serial.println(httpCode);
    Serial.println(payload);
    http.end();
  } else {
    Serial.println("Wifi connection failed, retrying.");   
    connectToWifiNetwork();
  }
}
 
void setup() {
  //Begin serial.
  Serial.begin(115200);
   dht.begin();
   //Setup pins.
   pinMode(WEATHERPIN, INPUT);
   pinMode(LDRPIN, INPUT);
   //Connect to wifi network
   connectToWifiNetwork();
   pinMode(WINDPIN, INPUT);
}
 
void loop() {
  //Read sensor data
   float humidity = dht.readHumidity();
   float temperature = dht.readTemperature();
   float lightIntensity = readLightInensity();
   float windSpeed = readWindSpeed();
   bool rain = readRain();
 
   StaticJsonBuffer<400> jsonBuffer;
   JsonObject& jsonRoot = jsonBuffer.createObject();
   
   //Initialize JSON object.
   jsonRoot["token"] = apiToken;
   JsonArray& data = jsonRoot.createNestedArray("data");
 
   //Humidity sensor data
   if(humidity == humidity) {
     JsonObject& humidityObject = jsonBuffer.createObject();
     humidityObject["key"] = "humidity";
     humidityObject["value"] = humidity;
     data.add(humidityObject);
   }
   //LDR sensor data
   if(lightIntensity == lightIntensity) {
     JsonObject& lightObject = jsonBuffer.createObject();
     lightObject["key"] = "light";
     lightObject["value"] = lightIntensity;
     data.add(lightObject);
   }
   //Temperature sensor data
   if(temperature == temperature) {
     JsonObject& temperatureObject = jsonBuffer.createObject();
     temperatureObject["key"] = "temperature";
     temperatureObject["value"] = temperature;
     data.add(temperatureObject);
   }
   //Rain / Snow sensor data
   if(rain == rain) {
     JsonObject& rainObject = jsonBuffer.createObject();
     rainObject["key"] = "precipitation";
     rainObject["value"] = rain;
     data.add(rainObject);
   }
  if(windSpeed == windSpeed) {
    JsonObject& windSpeedObject = jsonBuffer.createObject();
    windSpeedObject["key"] = "wind_speed";
    windSpeedObject["value"] = windSpeed;
    data.add(windSpeedObject);
  }
 
  String dataToSend;
  jsonRoot.printTo(dataToSend);
 
  // //Print json string to console.
  Serial.println(dataToSend);
 
  // //Post data to server.
  postData(dataToSend);
  delay(5000);
 
  Serial.println(readWindSpeed());
}
