<?php
	require "config.php";
	$return = [];

	$sql = "SELECT * FROM `ws_actuators`";

	if (isset($_GET["device_id"]) && !empty($_GET["device_id"])) {
		$sql .= " WHERE `device_id` = '" . $_GET["device_id"] . "'";
	}

	$result = $APIDB->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$tempActuator = [];
			$tempActuator["id"] = (int) $row["id"];
			$tempActuator["device_id"] = $row["device_id"];
			$tempActuator["value"] = (int) $row["value"];
			$tempActuator["received"] = (bool) $row["received"];
			array_push($return, $tempActuator);
		}
		if(count($return) == 1) {
			//$return = $return[0];

			$conn2 = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
			if ($conn2->connect_error) {
			    die("Connection failed: " . $conn->connect_error);
			} 

			$sql2 = "UPDATE `ws_actuators` SET `received` = TRUE WHERE `id` = '" . $return[0]["id"] . "'";
			$result2 = $APIDB->query($sql);
			if ($conn2->query($sql2) === TRUE) {
			} else {
				echo "fail";
				echo $APIDB->getError();
			}
		}
	} else {
		http_response_code(404); //not found
	}

	$encodedJson = json_encode($return);
	if(!$encodedJson) {
		utf8_encode_deep($return);
		$encodedJson = json_encode($return);
	}
	echo $encodedJson;
?>